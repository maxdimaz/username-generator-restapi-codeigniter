<?php
/**
 * Created by PhpStorm.
 * User: maxdimaz
 * Date: 2019-07-18
 * Time: 10:11
 */


defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . 'libraries/REST_Controller.php';

/**
 * User Controller
 * Controler untuk menangani User mulai dari generate username dan menyimpan
 *
 * @package         UserGeneratorDms
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Dimas Edy Prasetyo
 */
class User extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
    }

    public function username_generator_post()
    {
        $Name = $this->post('name');


        // Step 1. Clean Name from simbol ' and make it lowercase
        $CleanName = preg_replace("/[^ \w]+/", "", $Name);

        // Step 2. Getting First Name & LastName
        $GeneratedName=preg_replace("~\b(\p{L}+)\b(?:.+\b(\p{L}+)\b)?~", '$1 $2', $CleanName);

        // Step 3. Username format remove space
        $UserNamePre=preg_replace("/\s+/","",strtolower($GeneratedName));

        // Step 4. Checkin in DB for dublicate entry
        $DataUser = $this->Cache->GetCache("username-data");
        if(count($DataUser)==0){
            $DataUser=array();
        }

        // Step 5. Checking Duplicate Name
        $FilteredUser = array_filter($DataUser, function($user) use ($UserNamePre) {
            return ($user['unique']==$UserNamePre);
        });
        $CountDuplicate=count($FilteredUser);
        $UserNameExtend=($CountDuplicate==0?"":$CountDuplicate);

        // Step 6. Merger Username and UsernameExtend
        $UserName=$UserNamePre.$UserNameExtend;

        // Step 7. Save to DB
        $EntryData=array(array(
            "name"=>$Name,
            "username"=>$UserName,
            "unique"=>$UserNamePre,
        ));
        $DataUser=array_merge($DataUser,$EntryData);
        $this->Cache->SetCache("username-data",$DataUser,0);


        if(!$UserNamePre){
            $this->set_response([
                'status' => FALSE,
                'message' => 'Please try again!'
            ], REST_Controller::HTTP_NOT_FOUND);
        }else{
            $this->set_response(array(
                "username"=>$UserName,
                "message"=>"",
            ), REST_Controller::HTTP_OK);
        }
    }
    public function username_generator_get()
    {
        $UserName = $this->get('username');

        $DataUser = $this->Cache->GetCache("username-data");
        if(count($DataUser)==0){
            $DataUser=array();
        }

        $FilteredUser = array_filter($DataUser, function($user) use ($UserName) {
            return ($user['username']==$UserName);
        });

        if(!$UserName){
            $this->set_response([
                'status' => FALSE,
                'message' => 'Please try again!'
            ], REST_Controller::HTTP_NOT_FOUND);
        }else{
            $this->set_response(array(
                'status' => TRUE,
                "users"=>array_values($FilteredUser),
                "message"=>"",
            ), REST_Controller::HTTP_OK);
        }
    }
    public function sample_data_get()
    {
        $data=array(
            array(
                "name"=>"Dimas",
                "username"=>"dimas",
                "unique"=>"dimas"
            ),
            array(
                "name"=>"Dimas Edy",
                "username"=>"dimasedy",
                "unique"=>"dimasedy"
            ),
            array(
                "name"=>"Dimas Edy",
                "username"=>"dimasedy1",
                "unique"=>"dimasedy"
            ),
            array(
                "name"=>"Dimas Edy Prasetyo",
                "username"=>"dimas",
                "unique"=>"dimasedyprasetyo"
            ),
        );
        $DataUser = $this->Cache->GetCache("username-data");
        $this->set_response(array(
            "message"=>"Success",
        ), REST_Controller::HTTP_OK);
    }
}