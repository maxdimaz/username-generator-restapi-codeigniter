<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
    <link rel="stylesheet" href="/vs2015.css">
    <title>Test App</title>

    <style>

        html,body{
            background-image: url('/bg.jpg');
            background-size: cover;
            background-repeat: no-repeat;
            height: 100%;
            font-family: 'Numans', sans-serif;
        }

        .container{
            height: 100%;
            align-content: center;
        }

        .card{
            height: 370px;
            margin-top: auto;
            margin-bottom: auto;
            width: 400px;
            background-color: rgba(0,0,0,0.5) !important;
        }

        .social_icon span{
            font-size: 60px;
            margin-left: 10px;
            color: #FFC312;
        }

        .social_icon span:hover{
            color: white;
            cursor: pointer;
        }

        .card-header h3{
            color: white;
        }

        .social_icon{
            position: absolute;
            right: 20px;
            top: -45px;
        }

        .input-group-prepend span{
            width: 50px;
            background-color: #FFC312;
            color: black;
            border:0 !important;
        }

        input:focus{
            outline: 0 0 0 0  !important;
            box-shadow: 0 0 0 0 !important;

        }

        .remember{
            color: white;
        }

        .remember input
        {
            width: 20px;
            height: 20px;
            margin-left: 15px;
            margin-right: 5px;
        }

        .login_btn{
            color: black;
            background-color: #FFC312;
            width: 100px;
        }

        .login_btn:hover{
            color: black;
            background-color: white;
        }

        .links{
            color: white;
        }

        .links a{
            margin-left: 4px;
        }
    </style>
</head>
<body>


<div class="container">
    <div class="d-flex justify-content-center h-100">
        <div class="card">
            <div class="card-header">
                <h3>Generate Username</h3>
                <div class="d-flex justify-content-end social_icon">
                    <a href="#" data-toggle="modal" data-target="#srcModal"><span><i class="fas fa-code"></i></span></a>
                </div>
            </div>
            <div class="card-body">
                <form action="" id="frm-generate">
                    <div class="input-group form-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-user"></i></span>
                        </div>
                        <input type="text" class="form-control" placeholder="Fullname" id="txt-fullname">
                    </div>

                    <div class="form-group mb-5">
                        <input type="submit" value="Generate" class="btn btn-block login_btn" id="btn-generate">
                    </div>

                </form>
                <div class="input-group form-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text"><i class="fas fa-user"></i></span>
                    </div>
                    <input type="text" class="form-control" placeholder="Result" id="txt-result">
                </div>

            </div>
            <div class="card-footer">
                <div class="d-flex justify-content-center links">
                    Click icon Code for detail
                </div>
            </div>
            </div>
        </div>
    </div>
</div>


<!-- Modal -->
<div class="modal fade" id="srcModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Source Code</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <p>
                    API URL : <strong>api/user/username_generator</strong><br>
                    Method : POST<br>
                    Data : {name:"FULLNAME"}<br>
                    Desk : Generate Username<br>
                </p>
                <p>
                    API URL : <strong>api/user/username_generator</strong><br>
                    Method : GET<br>
                    Data : {username:"USERNAME"}<br>
                    Desk : Get data fromusername<br>
                </p>

                <h4>Source Code</h4>
                <pre>
                    <code class="php">


defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . 'libraries/REST_Controller.php';

/**
 * User Controller
 * Controler untuk menangani User mulai dari generate username dan menyimpan
 *
 * @package         UserGeneratorDms
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Dimas Edy Prasetyo
 */
class User extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
    }

    public function username_generator_post()
    {
        $Name = $this->post('name');


        // Step 1. Clean Name from simbol ' and make it lowercase
        $CleanName = preg_replace("/[^ \w]+/", "", $Name);

        // Step 2. Getting First Name & LastName
        $GeneratedName=preg_replace("~\b(\p{L}+)\b(?:.+\b(\p{L}+)\b)?~", '$1 $2', $CleanName);

        // Step 3. Username format remove space
        $UserNamePre=preg_replace("/\s+/","",strtolower($GeneratedName));

        // Step 4. Checkin in DB for dublicate entry
        $DataUser = $this->Cache->GetCache("username-data");
        if(count($DataUser)==0){
            $DataUser=array();
        }

        // Step 5. Checking Duplicate Name
        $FilteredUser = array_filter($DataUser, function($user) use ($UserNamePre) {
            return ($user['unique']==$UserNamePre);
        });
        $CountDuplicate=count($FilteredUser);
        $UserNameExtend=($CountDuplicate==0?"":$CountDuplicate);

        // Step 6. Merger Username and UsernameExtend
        $UserName=$UserNamePre.$UserNameExtend;

        // Step 7. Save to DB
        $EntryData=array(array(
            "name"=>$Name,
            "username"=>$UserName,
            "unique"=>$UserNamePre,
        ));
        $DataUser=array_merge($DataUser,$EntryData);
        $this->Cache->SetCache("username-data",$DataUser,0);


        if(!$UserName){
            $this->set_response([
                'status' => FALSE,
                'message' => 'Please try again!'
            ], REST_Controller::HTTP_NOT_FOUND);
        }else{
            $this->set_response(array(
                "username"=>$UserName,
                "message"=>"",
            ), REST_Controller::HTTP_OK);
        }
    }
    public function username_generator_get()
    {
        $UserName = $this->get('username');

        $DataUser = $this->Cache->GetCache("username-data");
        if(count($DataUser)==0){
            $DataUser=array();
        }

        $FilteredUser = array_filter($DataUser, function($user) use ($UserName) {
            return ($user['username']==$UserName);
        });

        if(!$UserName){
            $this->set_response([
                'status' => FALSE,
                'message' => 'Please try again!'
            ], REST_Controller::HTTP_NOT_FOUND);
        }else{
            $this->set_response(array(
                'status' => TRUE,
                "users"=>array_values($FilteredUser),
                "message"=>"",
            ), REST_Controller::HTTP_OK);
        }
    }
    public function sample_data_get()
    {
        $data=array(
            array(
                "name"=>"Dimas",
                "username"=>"dimas",
                "unique"=>"dimas"
            ),
            array(
                "name"=>"Dimas Edy",
                "username"=>"dimasedy",
                "unique"=>"dimasedy"
            ),
            array(
                "name"=>"Dimas Edy",
                "username"=>"dimasedy1",
                "unique"=>"dimasedy"
            ),
            array(
                "name"=>"Dimas Edy Prasetyo",
                "username"=>"dimas",
                "unique"=>"dimasedyprasetyo"
            ),
        );
        $DataUser = $this->Cache->GetCache("username-data");
        $this->set_response(array(
            "message"=>"Success",
        ), REST_Controller::HTTP_OK);
    }
}

                    </code>
                </pre>

            </div>
        </div>
    </div>
</div>

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<script src="//cdnjs.cloudflare.com/ajax/libs/highlight.js/9.15.8/highlight.min.js"></script>
<script>
    $(function (e) {
        hljs.initHighlightingOnLoad();
        $("#frm-generate").on("submit", function (e) {
            e.preventDefault();
        })
        $("#btn-generate").on("click", function (e) {
            $(this).addClass("disabled");
            $(this).val("Generating ...");
            $("#txt-result").val("");
            var FullName=$("#txt-fullname").val();
            //
            $.ajax({
                url: "api/user/username_generator",
                dataType: 'json',
                type: 'post',
                async: true,
                data:{name:FullName},
                success: function (output) {
                    $("#btn-generate").removeClass("disabled");
                    $("#btn-generate").val("Generate");
                    $("#txt-result").val(output.username);
                },
                error: function (e) {
                    $("#btn-generate").removeClass("disabled");
                    $("#btn-generate").val("Generate");
                }
            });



            setTimeout(function (e) {

            },5000)
        })
    })
</script>
</body>
</html>