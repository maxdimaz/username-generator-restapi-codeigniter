<?php
/**
 * Created by PhpStorm.
 * User: maxdimaz
 * Date: 2019-07-18
 * Time: 12:21
 */

defined('BASEPATH') OR exit('No direct script access allowed');
use phpFastCache\CacheManager;

 class Cache extends CI_Model {
    private $InstanceCache;

    public function __construct() {
        parent::__construct();

        /**START : - Setup Fastcache**/
        $cacheconf = array(
            "storage"=>"sqlite",
            'securityKey'=>"emailgen",
//            "path" => APPPATH."/cache/"
        );
        CacheManager::setDefaultConfig($cacheconf);
        $this->InstanceCache = CacheManager::getInstance("sqlite");
        /**END : - Setup Fastcache**/
    }

    public function SetCache($key,$value,$time=600,$tag=null){
        log_message("debug", "Cache Named : {{".$key."}}");
        $CachedString = $this->InstanceCache->getItem($key);
        $CachedString->set($value)->expiresAfter($time);//in seconds, also accepts Datetime
        if($tag){
            $CachedString->addTag($tag);
        }
        return $this->InstanceCache->save($CachedString);
    }

    public function GetCache($key){
        $CachedString = $this->InstanceCache->getItem($key);
        return $CachedString->get();
    }
    public function RemoveCache($key){
        return $this->InstanceCache->deleteItem($key);
    }
    public function CacheIsExist($key){
        $CachedString = $this->InstanceCache->getItem($key);
        return (is_null($CachedString->get())?false:true);
    }


}
